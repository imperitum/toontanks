// Fill out your copyright notice in the Description page of Project Settings.


#include "ToonTanks/GameModes/TankGameModeBase.h"
#include <Kismet/GameplayStatics.h>
#include <ToonTanks/Pawns/PawnTank.h>
#include <ToonTanks/Pawns/PawnTurret.h>
#include <ToonTanks/PlayerControllers/PlayerControllerBase.h>

void ATankGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	HandleGameStart();
}

int32 ATankGameModeBase::GetTurretsCount()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTurret::StaticClass(), LevelTurrets);
	return LevelTurrets.Num();
}

void ATankGameModeBase::HandleGameStart()
{
	PlayerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
	PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));
	TurretsCount = GetTurretsCount();

	if (PlayerControllerRef)
	{
		PlayerControllerRef->SetPlayerEnabledState(false);

		FTimerHandle PlayerEnabledHandle;
		FTimerDelegate PlayerEnabledDelegate = FTimerDelegate::CreateUObject(PlayerControllerRef, &APlayerControllerBase::SetPlayerEnabledState, true);
		GetWorld()->GetTimerManager().SetTimer(PlayerEnabledHandle, PlayerEnabledDelegate, float(StartDelay), false);
	}

	GameStart();
	UpdateScore(Score);
}

void ATankGameModeBase::HandleGameOver(bool bIsPlayerWon)
{
	PlayerControllerRef->SetShowMouseCursor(true);
	GameOver(bIsPlayerWon);
}

void ATankGameModeBase::ActorDied(AActor* DeadActor)
{
	if (DeadActor == PlayerTank)
	{
		PlayerTank->HandleDestruction();
		if (PlayerControllerRef) PlayerControllerRef->SetPlayerEnabledState(false);
		HandleGameOver(false);
	}
	else if (APawnTurret* DestroyedTurret = Cast<APawnTurret>(DeadActor))
	{
		DestroyedTurret->HandleDestruction();
		Score++;
		UpdateScore(Score);

		if (GetTurretsCount() == 0)
		{
			HandleGameOver(true);
		}
	}
}