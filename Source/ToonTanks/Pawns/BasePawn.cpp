// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePawn.h"
#include <Components/CapsuleComponent.h>
#include <ToonTanks/Actors/ProjectileBase.h>
#include <ToonTanks/Components/HealthComponent.h>
#include <Kismet/GameplayStatics.h>

// Sets default values
ABasePawn::ABasePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	RootComponent = CapsuleComponent;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	BaseMesh->SetupAttachment(RootComponent);

	TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Mesh"));
	TurretMesh->SetupAttachment(BaseMesh);

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	ProjectileSpawnPoint->SetupAttachment(TurretMesh);

	Health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
}

void ABasePawn::RotateTurret(FVector LookAtTarget)
{
	FVector CleanRotation = FVector(LookAtTarget.X, LookAtTarget.Y, TurretMesh->GetComponentLocation().Z);
	FVector StartLocation = TurretMesh->GetComponentLocation();

	FRotator TargetRotation = FVector(CleanRotation - StartLocation).Rotation();
	TurretMesh->SetWorldRotation(TargetRotation);
}

void ABasePawn::Fire()
{
	if (ProjectileClass)
	{
		FTransform SpawnPoint = ProjectileSpawnPoint->GetComponentTransform();
		AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(ProjectileClass, SpawnPoint);
		TempProjectile->SetOwner(this);
	}
}

void ABasePawn::HandleDestruction()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, DestroyParticles, GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(this, DestructionSound, GetActorLocation());
	GetWorld()->GetFirstPlayerController()->ClientStartCameraShake(DestructionShake);
}

