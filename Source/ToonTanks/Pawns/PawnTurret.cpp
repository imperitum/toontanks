// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTurret.h"
#include <Kismet/GameplayStatics.h>
#include "DrawDebugHelpers.h"
#include "PawnTank.h"

void APawnTurret::BeginPlay()
{
	Super::BeginPlay();

	PlayerPawn = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));

	SphereCollision = FCollisionShape::MakeSphere(7.f);

	TraceParams = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	TraceParams.bIgnoreBlocks = false;

	//reloading timer
	FireTimerDelegate.BindLambda([&]()
	{
		bCanFire = true;
	});
}

void APawnTurret::HandleDestruction()
{
	Super::HandleDestruction();
	Destroy();
}

void APawnTurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (PlayerPawn && PlayerPawn->GetIsPlayerAlive() && GetDistanceToPlayer() <= FireRange)
	{
		RotateTurret(PlayerPawn->GetActorLocation());

		if (bCanFire && IsTankVisible())
		{
			Fire();
			Reloading();
		}
	}
}

void APawnTurret::Reloading()
{
	bCanFire = false;
	GetWorld()->GetTimerManager().SetTimer(FireRateTimerHandle, FireTimerDelegate, FireRate, false);
}

bool APawnTurret::IsTankVisible()
{
	FHitResult Hit;

	FVector StartLocation = ProjectileSpawnPoint->GetComponentLocation();
	FVector EndLocation = PlayerPawn->GetActorLocation();

	/*const FName TraceTag("gasdfs");
	GetWorld()->DebugDrawTraceTag = TraceTag;
	FCollisionQueryParams CollisionParams;
	CollisionParams.TraceTag = TraceTag;*/

	/*GetWorld()->LineTraceSingleByChannel(
		Hit,
		StartLocation,
		EndLocation,
		ECollisionChannel::ECC_Visibility,
		TraceParams
	);*/

	GetWorld()->SweepSingleByChannel(
		Hit,
		StartLocation,
		EndLocation,
		{ 0.f, 0.f, 0.f, 0.f },
		ECollisionChannel::ECC_Visibility,
		SphereCollision,
		TraceParams
	);

	if (Hit.GetActor() == PlayerPawn)
	{
		return true;
	}
	else
	{
		return false;
	}
}

float APawnTurret::GetDistanceToPlayer()
{
	if (!PlayerPawn) return 0.f;

	return FVector::Dist(PlayerPawn->GetActorLocation(), GetActorLocation());
}