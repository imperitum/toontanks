// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "PawnTurret.generated.h"

class APawnTank;

UCLASS()
class TOONTANKS_API APawnTurret : public ABasePawn
{
	GENERATED_BODY()
	
private:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	float FireRate = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	float FireRange = 1000.f;

	FTimerHandle FireRateTimerHandle;
	FTimerDelegate FireTimerDelegate;
	FCollisionShape SphereCollision;
	FCollisionQueryParams TraceParams;
	bool bCanFire = true;

	APawnTank* PlayerPawn;

	void Reloading();
	bool IsTankVisible();
	float GetDistanceToPlayer();

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void HandleDestruction() override;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
